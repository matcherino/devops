const _ = require('lodash')
const {promisify} = require('util')
const deepmerge = require('./extend').deepExtend
const execa = require('execa')
const fp = require('path')
const fs = require('fs')
const globby = require('globby')
const minimist = require('minimist')
const resolveFrom = require('resolve-from')
const stripComments = require('strip-json-comments')
const tmp = require('tmp')
const untildify = require('untildify')
const isBinaryFile = require('is-binary-path')
const inquirer = require('inquirer')
const randomkey = require('randomkey')
const fjstringify = require('fast-json-stable-stringify')

const log = require('loglevel')

log.setDefaultLevel('debug')

const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)

const parsePairs = s => {
  const obj = {}
  if (!s) return obj

  const pairs = s.split(/\s+/)
  if (!pairs || pairs.length < 1) {
    throw new Error('Cannot parse pairs on string: ' + s)
  }

  for (const pair of pairs) {
    const idx = pair.indexOf('=')
    if (idx < 0) throw new Error('Invalid key=value pair: ' + pair)
    obj[pair.substr(0, idx)] = pair.substr(idx + 1)
  }
  return obj
}

const argv = minimist(process.argv.slice(2), {
  string: ['confEnv'],
})

/**
 * Executes a BASH shell script.
 */
const bash = async (
  script,
  options = {stdio: 'inherit', shell: '/bin/bash'}
) => {
  return execa(script, options)
}

// evalFile evalutes a Javascript or JSON file
const evalFile = async (originalFilename, ...args) => {
  const filename = untildify(originalFilename)
  const ext = fp.extname(filename)

  if (!fs.existsSync(filename)) {
    throw new Error(`File does not exist: ${filename}`)
  }

  if (ext === '.json') {
    const raw = await readFile(filename, 'utf8')
    const stripped = stripComments(raw)
    return JSON.parse(stripped)
  } else if (ext === '.js') {
    const objOrFn = require(fp.resolve(filename))
    if (typeof objOrFn === 'function') {
      return await objOrFn(...args)
    }
    return objOrFn
  }
  throw new Error('Context file must be .js or .json: ' + filename)
}

const renderAngleBrackets = (s, context) => {
  if (!s || !context || s.indexOf('<<') < 0) return s

  var compiled = _.template(s, {interpolate: /<<([\s\S]+?)>>/g})
  return compiled(context)
}

/**
 * Writes a temporary text file.
 */
const writeTempFile = async (content, tmpOptions = {}) => {
  return new Promise((resolve, reject) => {
    tmp.file(tmpOptions, (err, path, fd, _cleanupCallback) => {
      if (err) {
        reject(err)
        return
      }

      fs.write(fd, content, err => {
        if (err) {
          reject(err)
          return
        }
        resolve(path)
      })

      // If we don't need the file anymore we could manually call the cleanupCallback
      // But that is not necessary if we didn't pass the keep option because the library
      // will clean after itself.
      //cleanupCallback();
    })
  })
}

// Prettifies a JSON object with 2-space indents
const prettify = o => fjstringify(o)

// Strigifies a JSON object.
const stringify = o => fjstringify(o)

const randomString = (len, alphabet = randomkey.safe) => {
  return randomkey(len, alphabet)
}

const loadJSONFile = async (filename, options) => {
  const {mustExist} = {
    mustExist: true,
    ...options,
  }
  const path = fp.resolve(filename)

  const exists = fs.existsSync(path)
  if (!exists) {
    if (mustExist) {
      throw `File does not exist: ${path}`
    }
    return {}
  }

  const content = await readFile(path, 'utf-8')
  return JSON.parse(stripComments(content))
}

module.exports = {
  _,
  argv,
  bash,
  evalFile,
  execa,
  globby,
  inquirer,
  isBinaryFile,
  loadJSONFile,
  log,
  merge: deepmerge,
  minimist,
  parsePairs,
  prettify,
  randomString,
  readFile,
  renderAngleBrackets,
  resolveFrom,
  sh: execa,
  stringify,
  untildify,
  writeFile,
  writeTempFile,
}
