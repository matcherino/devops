# @mgutz/devops

General packages and utilities for devops.

## Executables

- conf - Merges configuration files

```sh
conf --confEnv test some.rc.js
```

- render-angles - Renders `<<angle_bracket>>` templates

## Exported Packages

- deepmerge (merge)
- execa
- lodash (`_`)
- minimist (argv)
