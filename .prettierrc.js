// prettier.config.js or .prettierrc.js
module.exports = {
  arrowParens: 'avoid',
  bracketSpacing: false,
  jsxBracketSameLine: false,
  semi: false,
  singleQuote: true,
  tabWidth: 2,
  useTabs: false,
  trailingComma: 'es5',
};
