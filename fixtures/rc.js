module.exports = {
  cwd: '~/go/src/matcherino/apiserver/configs',
  pipeline: [
    {
      jsonFile: 'common.private.json'
    },
    {
      jsonFile: '~/matcherino/reactui/conf_env/development/reactui-server.json',
      when: "conf_env == 'development'",
      dest: '__clientConfig'
    },
    {
      jsonFile: 'test.private.json',
      when: "conf_env == 'test'"
    },
    {
      jsonFile: '~/matcherino/reactui/conf_env/test/reactui-server.json',
      when: "conf_env == 'test'",
      dest: '__clientConfig'
    },
    {
      jsonFile: 'staging.private.json',
      when: "conf_env == 'staging'"
    },
    {
      jsonFile: '~/matcherino/reactui/conf_env/staging/reactui-server.json',
      when: "conf_env == 'staging'",
      dest: '__clientConfig'
    },
    {
      jsonFile: 'production.private.json',
      when: "conf_env == 'production'"
    },
    {
      jsonFile: '~/matcherino/reactui/conf_env/production/reactui-server.json',
      when: "conf_env == 'production'",
      dest: '__clientConfig'
    }
  ]
};
