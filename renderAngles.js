#!/usr/bin/env node

const _ = require('lodash')
const fs = require('fs')
const {promisify} = require('util')
const pkg = require('./package')
const readFile = promisify(fs.readFile)
const renderer = require('./renderer')

const usage = () => {
  return `render-angles v${pkg.version}
usage: render-angles <text_file> options

options:
  --overwrite     Overwrite input file (in-place)
  --output        Output file
  --context       Context .js or .json file
  --set           Set JSON path value

examples:

# override context values
render-angles template --set some.path=foo --set another.path=bar
`
}

// Renders template using '<<' and '>>'
const render = async (templateFile, context) => {
  const template = await readFile(templateFile, 'utf8')
  var compiled = _.template(template, {interpolate: /<<([\s\S]+?)>>/g})
  return compiled(context)
}

const main = async () => {
  await renderer(render, usage)
}

main()
