#!/usr/bin/env node
const _ = require('lodash')
const minimist = require('minimist')
const merge = require('./extend').deepExtend
const argv = minimist(process.argv.slice(2), {
  boolean: ['overwrite'],
})
const pkg = require('./package')
const untildify = require('untildify')
const jexl = require('jexl')

const {evalFile, loadJSONFile} = require('./util')

const usage = () => {
  return `conf v${pkg.version}
usage: conf RCFILE
`
}

const loadRc = async () => {
  const rcFile = argv._[0]
  if (!rcFile) {
    throw new Error(usage())
  }
  return await evalFile(rcFile, argv)
}

const writeOutput = async (obj, replacer) => {
  let s = JSON.stringify(obj, null, 2)
  if (typeof replacer === 'function') {
    s = await replacer(argv, s)
  }
  console.log(s)
}

const main = async () => {
  const rc = await loadRc()
  let config = {}

  if (rc.cwd) {
    process.chdir(untildify(rc.cwd))
  }

  for (const item of rc.pipeline) {
    const {jsonFile, value, when, mustExist} = item

    if (when) {
      const truthy = await jexl.eval(when, argv)
      if (!truthy) {
        continue
      }
    }

    let v = {}
    if (jsonFile) {
      const filename = untildify(jsonFile)
      v = await loadJSONFile(filename, {mustExist})
    } else if (value) {
      v = value
      if (!_.isPlainObject(value) && !item.set) {
        throw new Error(
          `Scalar values require 'set' prop: ${JSON.stringify(item)}`
        )
      }
    }

    if (item.get) {
      v = _.get(v, item.get)
    }

    if (item.set) {
      const path = item.set
      _.set(config, path, v)
      continue
    } else if (item.merge) {
      const path = item.merge
      const original = _.get(config, path)
      _.set(config, path, merge(original, v))
      continue
    }

    config = merge(config, v)
  }

  await writeOutput(config, rc.replacer)
}

main()
