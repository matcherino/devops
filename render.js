#!/usr/bin/env node

const {evalFile} = require('./util')
const pkg = require('./package')
const renderer = require('./renderer')

const usage = () => {
  return `render v${pkg.version} - renders exported function or object

usage: render <js_file> options

options:
  --overwrite     Overwrite input file (in-place)
  --output        Output file
  --context       Context .js or .json file
  --set           Set JSON path value

examples:

# override context values
render some.js --set some.path=foo --set another.path=bar
`
}

const render = async (templateFile, context) => {
  return await evalFile(templateFile, context)
}

const main = async () => {
  await renderer(render, usage)
}

main()
