const {prettify} = require('./util')

/**
 * Computes configuration environment (confEnv) and server suffix from environment
 * variables.
 *
 * Prints `${confEnv} ${confEnvHost} ${serverSuffix}`
 *
 * In bash
 * 	IFS=', ' read -r -a your_var <<< $(compute-cenv)
 * 	echo "${your_var[0]}"
 */

const mustConfEnv = opts => {
  if (opts && !opts.namespace) {
    throw `mustConfEnv argument ops, 'namespace' prop is required: ${prettify(
      opts
    )}`
  } else {
    if (!process.env.namespace) {
      throw `env variable is required: namespace`
    }
  }

  const obj = opts && (opts.confEnv || opts.namespace) ? opts : process.env

  let {confEnv, namespace} = obj

  if (!confEnv && !namespace) {
    confEnv = 'development'
  }

  let cenv,
    num = ''
  if (namespace) {
    const matches = namespace.match(/([a-z]+)([0-9]*)/)
    cenv = matches[1]
    // namespace=staging becomes staging1
    num = matches[2] || '1'
  }

  if (confEnv) {
    cenv = confEnv
  }

  const allowed = ['development', 'test', 'staging', 'production']
  if (allowed.indexOf(cenv) === -1) {
    console.error(`Invalid confEnv value: ${JSON.stringify(confEnv)}`)
    process.exit(1)
  }

  num = safeParseInt(num)

  // num needs to be 01, 02 .. n indicating host server suffix
  // legacy logic 'staging' -> staging-01
  if (num === 0) {
    num = 1
  }

  const serverSuffix = num.toString().padStart(2, '0')
  const confEnvHost = `${cenv}-app-${serverSuffix}`
  const ns = `${cenv}${parseInt(serverSuffix, 10)}`

  // eg {
  //  confEnv: 'production',
  //  confEnvHost: 'production-app-01',
  //  confEnvHostSeq: '01',
  //  namespace: 'production1'
  //  }
  return {
    confEnv: cenv,
    confEnvHost,
    confEnvHostSeq: serverSuffix,
    namespace: ns,
  }
}

function safeParseInt(s) {
  const n = parseInt(s)
  return isNaN(n) ? 0 : n
}

module.exports = {
  mustConfEnv,
}
