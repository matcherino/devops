const _ = require('lodash')
const minimist = require('minimist')
const merge = require('./extend').deepExtend
const argv = minimist(process.argv.slice(2), {
  string: ['set'],
  boolean: ['overwrite'],
})
const pkg = require('./package')
const untildify = require('untildify')

const {evalFile} = require('./util')

// __.untildify()
const helpers = {
  untildify,
}

const mutateContextWithSets = context => {
  if (!argv.set) return context

  const values = Array.isArray(argv.set) ? argv.set : [argv.set]
  for (const s of values) {
    const idx = s.indexOf('=')
    if (idx < 0) {
      console.error(`Unable to set value for expression: --set ${s}`)
      process.exit(1)
    }

    const path = s.substr(0, idx)
    const value = s.substr(idx + 1)
    _.set(context, path, value)
  }

  context.__ = helpers
  return context
}

const mergeContexts = async () => {
  const filenames = argv.context
  let context = {}
  if (!filenames) return context

  const files = Array.isArray(filenames) ? filenames : [filenames]
  for (const filename of files) {
    const ctx = await evalFile(filename)
    context = merge(context, ctx)
  }
  return context
}

const getInputFile = () => {
  return argv._[0]
}

const writeOutput = async rendered => {
  const outputFile = argv.overwrite ? getInputFile() : argv.output

  if (outputFile) {
    await writeFile(outputFile, rendered, 'utf8')
    return
  }
  console.log(rendered)
}

module.exports = async (renderFn, usageFn) => {
  const template = argv._[0]
  if (!template) {
    console.log(usageFn())
    return 1
  }

  const context = await mergeContexts()
  mutateContextWithSets(context)
  const rendered = await renderFn(template, context)
  await writeOutput(rendered)
}
